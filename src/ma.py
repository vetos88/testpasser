class MyError(Exception):
    pass


class First(object):
    def __init__(self):
        pass


class Second(First):
    def __init__(self):
        First.__init__(self)


class Parent(Second):
    def __init__(self):
        First.__init__(self)


class A(Parent):

    _isSecond = None

    def __init__(self):
        First.__init__(self)
        self.i = 3
        self._isSecond = 0

    @property
    def isSecond(self):
        return self._isSecond

    @isSecond.getter
    def isSecond(self):
        return self._isSecond

    def fnc(self, a, b=None):
        if a and b:
            return 10 * 4 * 5
        elif a == 7:
            raise MyError("Error text")
        return 2 * 2 * 3

    def isFirst(self):
        return 1
